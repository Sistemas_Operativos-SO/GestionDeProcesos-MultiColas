package vista.componentes.panelProcesosEnCola;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.JScrollPane;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import logica.Proceso;

public class ProcesosEnColaTemplate extends JPanel {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private JScrollPane scrollProcesosRoundRobin, scrollProcesosBloqueados, scrollColaFCFS, scrollColaSJF;
    private JLabel lProcesosRoundRobin, lProcesosBloqueados, lProcesosFCFS, lProcesosSJF;
    private JButton bAddProcesos, bBloqueados, bDesbloquear, bAddColSJF, bAddColFCFS, bColaBloqueados;

    private Border borderT;
    private Font fuente, fuente17;
    private ImageIcon iAux, iBloquear, iDesbloquear, iAnadir, iCambio, iCambio2, iAnadir2, iCambioV, iCambioV2;
    private Color colorAzul;
    private Cursor cMano;

    private ArrayList<Proceso> procesosRoundRobin, procesosBloqueados, procesosFCFS, procesosSJF;
    private Iterator<Proceso> iT;
    private int x, y, iterador;

    private ProcesosEnColaComponent procesosEnColaComponent;

    public ProcesosEnColaTemplate(
        ProcesosEnColaComponent procesosEnColaComponent, ArrayList<Proceso> procesosRoundRobin, 
        ArrayList<Proceso> procesosBloqueados, ArrayList<Proceso> procesosMenorRafaga, ArrayList<Proceso> procesosFCFS) {

        this.procesosRoundRobin = procesosRoundRobin;
        this.procesosBloqueados = procesosBloqueados;
        this.procesosSJF = procesosMenorRafaga;
        this.procesosFCFS = procesosFCFS;
        this.procesosEnColaComponent = procesosEnColaComponent;

        crearObjetosDecoradores();
        crearBotones();
        crearRoundRobin();
        crearColaBloqueados();
        crearFIFO();
        crearMenorRafaga();

        this.setLayout(null);
        this.setSize(1350, 100);
        this.setBackground(null);
    }

    private void crearColaBloqueados() {
        scrollProcesosBloqueados = new JScrollPane();
        scrollProcesosBloqueados.setBounds(70, 0, 410, 100);
        scrollProcesosBloqueados.setBackground(null);
        scrollProcesosBloqueados.setBorder(BorderFactory.createTitledBorder(borderT, "Cola de bloqueados",
                SwingConstants.LEFT, 0, fuente17, Color.WHITE));
        this.add(scrollProcesosBloqueados);

        lProcesosBloqueados = new JLabel() {

            private static final long serialVersionUID = 1L;

            public void paint(Graphics grph) {
                Graphics2D g2d = (Graphics2D) grph;
                grph.setFont(fuente);
                grph.setColor(new Color(50, 50, 51));
                grph.fillRect(0, 0, 440, 80);
                dibujaColaBloqueados(g2d);
            }
        };
        lProcesosBloqueados.setPreferredSize(new Dimension(390, 65));
        scrollProcesosBloqueados.setViewportView(lProcesosBloqueados);
        scrollProcesosBloqueados.setVisible(false);
    }

    private void dibujaColaBloqueados(Graphics grph) {
        int anchura = procesosBloqueados.size();
        if (anchura == 0)
            return;
        y = 52;
        x = (anchura * 98) + 7;
        iterador = 0;
        lProcesosBloqueados.setPreferredSize(new Dimension(x, y));
        grph.setColor(new Color(50, 50, 51));
        grph.fillRect(0, 0, x, y);
        x = 10;
        y = 2;
        iT = procesosBloqueados.iterator();
        Proceso proceso;

        while (iT.hasNext()) {
            if (iterador == procesosBloqueados.size())
                break;

            proceso = procesosBloqueados.get(iterador);

            grph.setColor(colorAzul);
            grph.fillRect(x - 2, y - 2, 92, 54);

            grph.setColor(proceso.getColor());
            grph.fillRect(x, y, 88, 50);

            grph.setColor(Color.WHITE);
            grph.drawString("Proceso " + proceso.getNombre(), x + 5, y + 15);
            grph.drawString("Rafaga: " + proceso.getRafaga(), x + 5, y + 30);
            grph.drawString("Enveje: " + proceso.getEnvejecimiento(), x + 5, y + 45);
            iterador++;
            x += 98;
        }
        scrollProcesosBloqueados.doLayout();
    }

    private void crearFIFO() {
        scrollColaFCFS = new JScrollPane();
        scrollColaFCFS.setBounds(938, 0, 410, 100);
        scrollColaFCFS.setBackground(null);
        scrollColaFCFS.setBorder(
                BorderFactory.createTitledBorder(borderT, "Cola FCFS", SwingConstants.LEFT, 0, fuente17, Color.WHITE));
        this.add(scrollColaFCFS);

        lProcesosFCFS = new JLabel() {
            /**
             *
             */
            private static final long serialVersionUID = 1L;

            public void paint(Graphics grph) {
                Graphics2D g2d = (Graphics2D) grph;
                grph.setFont(fuente);
                grph.setColor(new Color(50, 50, 51));
                grph.fillRect(0, 0, 440, 80);
                dibujaColaFIFO(g2d);
            }
        };
        lProcesosFCFS.setPreferredSize(new Dimension(390, 65));
        scrollColaFCFS.setViewportView(lProcesosFCFS);
    }

    protected void dibujaColaFIFO(Graphics2D g2d) {
        int anchura = procesosFCFS.size();
        if (anchura == 0)
            return;
        y = 52;
        x = (anchura * 98) + 7;
        iterador = 0;
        lProcesosFCFS.setPreferredSize(new Dimension(x, y));
        g2d.setColor(new Color(50, 50, 51));
        g2d.fillRect(0, 0, x, y);
        x = 10;
        y = 2;
        iT = procesosFCFS.iterator();
        Proceso proceso;

        while (iT.hasNext()) {
            if (iterador == procesosFCFS.size())
                break;

            proceso = procesosFCFS.get(iterador);

            g2d.setColor(colorAzul);
            g2d.fillRect(x - 2, y - 2, 92, 54);

            g2d.setColor(proceso.getColor());
            g2d.fillRect(x, y, 88, 50);

            g2d.setColor(Color.WHITE);
            g2d.drawString("Proceso " + proceso.getNombre(), x + 5, y + 15);
            g2d.drawString("Rafaga: " + proceso.getRafaga(), x + 5, y + 30);
            g2d.drawString("Enveje: " + proceso.getEnvejecimiento(), x + 5, y + 45);
            iterador++;
            x += 98;
        }
        scrollColaFCFS.doLayout();
    }

    private void crearMenorRafaga() {
        scrollColaSJF = new JScrollPane();
        scrollColaSJF.setBounds(504, 0, 410, 100);
        scrollColaSJF.setBackground(null);
        scrollColaSJF.setBorder(BorderFactory.createTitledBorder(borderT, "Cola SJF",
                SwingConstants.LEFT, 0, fuente17, Color.WHITE));
        this.add(scrollColaSJF);

        lProcesosSJF = new JLabel() {
            /**
             *
             */
            private static final long serialVersionUID = 1L;

            public void paint(Graphics grph) {
                Graphics2D g2d = (Graphics2D) grph;
                grph.setFont(fuente);
                grph.setColor(new Color(50, 50, 51));
                grph.fillRect(0, 0, 440, 80);
                dibujaColaMenorRafaga(g2d);
            }
        };
        lProcesosSJF.setPreferredSize(new Dimension(390, 65));
        scrollColaSJF.setViewportView(lProcesosSJF);
    }

    protected void dibujaColaMenorRafaga(Graphics2D g2d) {
        int anchura = procesosSJF.size();
        if (anchura == 0)
            return;
        y = 52;
        x = (anchura * 98) + 7;
        iterador = 0;
        lProcesosSJF.setPreferredSize(new Dimension(x, y));
        g2d.setColor(new Color(50, 50, 51));
        g2d.fillRect(0, 0, x, y);
        x = 10;
        y = 2;
        iT = procesosSJF.iterator();
        Proceso proceso;

        while (iT.hasNext()) {
            if (iterador == procesosSJF.size())
                break;

            proceso = procesosSJF.get(iterador);

            g2d.setColor(colorAzul);
            g2d.fillRect(x - 2, y - 2, 92, 54);

            g2d.setColor(proceso.getColor());
            g2d.fillRect(x, y, 88, 50);

            g2d.setColor(Color.WHITE);
            g2d.drawString("Proceso " + proceso.getNombre(), x + 5, y + 15);
            g2d.drawString("Rafaga: " + proceso.getRafaga(), x + 5, y + 30);
            g2d.drawString("Enveje: " + proceso.getEnvejecimiento(), x + 5, y + 45);
            iterador++;
            x += 98;
        }
        scrollColaSJF.doLayout();
    }

    private void crearRoundRobin() {
        scrollProcesosRoundRobin = new JScrollPane();
        scrollProcesosRoundRobin.setBounds(70, 0, 410, 100);
        scrollProcesosRoundRobin.setBackground(null);
        scrollProcesosRoundRobin.setBorder(BorderFactory.createTitledBorder(borderT, "Cola Round Robin",
                SwingConstants.LEFT, 0, fuente17, Color.WHITE));
        this.add(scrollProcesosRoundRobin);

        lProcesosRoundRobin = new JLabel() {
            /**
             *
             */
            private static final long serialVersionUID = 1L;

            public void paint(Graphics grph) {
                Graphics2D g2d = (Graphics2D) grph;
                grph.setFont(fuente);
                grph.setColor(new Color(50, 50, 51));
                grph.fillRect(0, 0, 440, 80);
                dibujaColaRoundRobin(g2d);
            }
        };
        lProcesosRoundRobin.setPreferredSize(new Dimension(390, 65));
        scrollProcesosRoundRobin.setViewportView(lProcesosRoundRobin);

    }

    private void dibujaColaRoundRobin(Graphics grph) {

        int anchura = procesosRoundRobin.size();
        if (anchura == 0)
            return;
        y = 52;
        x = (anchura * 98) + 7;
        iterador = 0;
        lProcesosRoundRobin.setPreferredSize(new Dimension(x, y));
        grph.setColor(new Color(50, 50, 51));
        grph.fillRect(0, 0, x, y);
        x = 10;
        y = 2;
        iT = procesosRoundRobin.iterator();
        Proceso proceso;

        while (iT.hasNext()) {
            if (iterador == procesosRoundRobin.size())
                break;

            proceso = procesosRoundRobin.get(iterador);

            grph.setColor(colorAzul);
            grph.fillRect(x - 2, y - 2, 92, 54);

            grph.setColor(proceso.getColor());
            grph.fillRect(x, y, 88, 50);

            grph.setColor(Color.WHITE);
            grph.drawString("Proceso " + proceso.getNombre(), x + 5, y + 15);
            grph.drawString("Rafaga: " + proceso.getRafaga(), x + 5, y + 30);
            grph.drawString("Enveje: " + proceso.getEnvejecimiento(), x + 5, y + 45);
            iterador++;
            x += 98;
        }
        scrollProcesosRoundRobin.doLayout();
    }

    private void crearObjetosDecoradores() {
        fuente = new Font("Impact", Font.PLAIN, 13);
        fuente17 = new Font("Impact", Font.PLAIN, 17);
        borderT = BorderFactory.createMatteBorder(1, 1, 1, 1, Color.LIGHT_GRAY);
        iBloquear = new ImageIcon("resources/img/bloquear.png");
        iDesbloquear = new ImageIcon("resources/img/desbloquear.png");
        iAnadir = new ImageIcon("resources/img/anadir.png");
        iAnadir2 = new ImageIcon("resources/img/anadir1.png");
        iCambio = new ImageIcon("resources/img/cambiarPestana.png");
        iCambio2 = new ImageIcon("resources/img/cambiarPestana1.png");
        iCambioV = new ImageIcon("resources/img/cambiarPestanaV.png");
        iCambioV2 = new ImageIcon("resources/img/cambiarPestanaV1.png");
        colorAzul = new Color(25, 93, 150);
        cMano = new Cursor(Cursor.HAND_CURSOR);
    }

    private void crearBotones() {

        bAddProcesos = new JButton("<html><div align='center'>Agregar Procesos</div></html>");
        bAddProcesos.setBounds(25, 40, 20, 20);
        bAddProcesos.setFocusable(false);
        bAddProcesos.setContentAreaFilled(false);
        bAddProcesos.addMouseListener(procesosEnColaComponent);
        bAddProcesos.setFont(fuente);
        iAux = new ImageIcon(iAnadir.getImage().getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
        bAddProcesos.setIcon(iAux);
        bAddProcesos.setForeground(Color.WHITE);
        bAddProcesos.setBackground(null);
        bAddProcesos.setBorder(null);
        bAddProcesos.setCursor(cMano);
        this.add(bAddProcesos);

        bBloqueados = new JButton();
        bBloqueados.setBounds(10, 70, 20, 20);
        bBloqueados.setFocusable(false);
        bBloqueados.setContentAreaFilled(false);
        bBloqueados.addMouseListener(procesosEnColaComponent);
        bBloqueados.setFont(fuente);
        bBloqueados.setForeground(Color.WHITE);
        bBloqueados.setBackground(null);
        bBloqueados.setCursor(cMano);
        iAux = new ImageIcon(iBloquear.getImage().getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
        bBloqueados.setIcon(iAux);
        bBloqueados.setBorder(null);
        this.add(bBloqueados);

        bDesbloquear = new JButton();
        bDesbloquear.setBounds(40, 70, 20, 20);
        bDesbloquear.setFocusable(false);
        bDesbloquear.setContentAreaFilled(false);
        bDesbloquear.addMouseListener(procesosEnColaComponent);
        bDesbloquear.setFont(fuente);
        bDesbloquear.setForeground(Color.WHITE);
        bDesbloquear.setBackground(null);
        bDesbloquear.setCursor(cMano);
        iAux = new ImageIcon(iDesbloquear.getImage().getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
        bDesbloquear.setIcon(iAux);
        bDesbloquear.setBorder(null);
        this.add(bDesbloquear);

        bAddColSJF = new JButton();
        bAddColSJF.setBounds(482, 40, 20, 20);
        bAddColSJF.setContentAreaFilled(false);
        iAux = new ImageIcon(iAnadir.getImage().getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
        bAddColSJF.setIcon(iAux);
        bAddColSJF.setFocusable(false);
        bAddColSJF.setBorder(null);
        bAddColSJF.addMouseListener(procesosEnColaComponent);
        bAddColSJF.setCursor(cMano);
        this.add(bAddColSJF);

        bColaBloqueados = new JButton();
        bColaBloqueados.setBounds(25, 12, 20, 20);
        bColaBloqueados.addMouseListener(procesosEnColaComponent);
        bColaBloqueados.setBackground(null);
        bColaBloqueados.setCursor(cMano);
        iAux = new ImageIcon(iCambio.getImage().getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
        bColaBloqueados.setIcon(iAux);
        bColaBloqueados.setBorder(null);
        bColaBloqueados.setContentAreaFilled(false);
        this.add(bColaBloqueados);

        bAddColFCFS = new JButton();
        bAddColFCFS.setBounds(916, 40, 20, 20);
        bAddColFCFS.setContentAreaFilled(false);
        iAux = new ImageIcon(iAnadir.getImage().getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
        bAddColFCFS.setIcon(iAux);
        bAddColFCFS.setFocusable(false);
        bAddColFCFS.setBorder(null);
        bAddColFCFS.addMouseListener(procesosEnColaComponent);
        bAddColFCFS.setCursor(cMano);
        this.add(bAddColFCFS);
    }

    public void cambiarVista() {
        if (scrollProcesosRoundRobin.isVisible()) {

            scrollProcesosRoundRobin.setVisible(false);
            scrollProcesosBloqueados.setVisible(true);
            iAux = new ImageIcon(iCambio2.getImage().getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
            bColaBloqueados.setIcon(iAux);
            scrollProcesosBloqueados.repaint();
        } else {

            scrollProcesosBloqueados.setVisible(false);
            scrollProcesosRoundRobin.setVisible(true);
            iAux = new ImageIcon(iCambio.getImage().getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
            bColaBloqueados.setIcon(iAux);
            scrollProcesosRoundRobin.repaint();
        }
    }

    public void actualizar() {
        scrollProcesosRoundRobin.repaint();
        scrollColaSJF.repaint();
        scrollColaFCFS.repaint();
    }

    public void actualizarBloqueados() {
        lProcesosBloqueados.repaint();
        lProcesosBloqueados.updateUI();
    }

    public JButton getbBloqueados() {
        return bBloqueados;
    }

    public JButton getbAddProcesos() {
        return bAddProcesos;
    }

    public JButton getbDesbloquear() {
        return bDesbloquear;
    }

    public JButton getbColaBloqueados() {
        return bColaBloqueados;
    }

    public JScrollPane getScrollProcesosRoundRobin() {
        return scrollProcesosRoundRobin;
    }

    public JScrollPane getScrollProcesosBloqueados() {
        return scrollProcesosBloqueados;
    }

    public JScrollPane getScrollColaFIFO() {
        return scrollColaFCFS;
    }

    public JScrollPane getScrollColaMenorRafaga() {
        return scrollColaSJF;
    }

    public JLabel getlProcesosRoundRobin() {
        return lProcesosRoundRobin;
    }

    public JButton getbAddColMenorRagafa() {
        return bAddColSJF;
    }

    public JButton getbAddColFIFO() {
        return bAddColFCFS;
    }

    public void botonAnadir2(JButton b) {
        iAux = new ImageIcon(iAnadir2.getImage().getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
        b.setIcon(iAux);
    }

    public void botonAnadir(JButton b) {
        iAux = new ImageIcon(iAnadir.getImage().getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
        b.setIcon(iAux);
    }

    public void botonPasarPestana(boolean pes) {
        if (pes){
            
            iAux = new ImageIcon(iCambio2.getImage().getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
            bColaBloqueados.setIcon(iAux);
        } else {
            
            iAux = new ImageIcon(iCambio.getImage().getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
            bColaBloqueados.setIcon(iAux);
        }
    }

    public void botonPasarPestana2(boolean pes) {
        if (pes){
            
            iAux = new ImageIcon(iCambioV2.getImage().getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
            bColaBloqueados.setIcon(iAux);
        } else {    
            
            iAux = new ImageIcon(iCambioV.getImage().getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
            bColaBloqueados.setIcon(iAux);
        }
    }

}